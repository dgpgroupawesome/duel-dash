﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class AttackBehaviours : MonoBehaviour {

	public int bucklerSpeed;
	public int scimitarSpeed;

	public int bucklerLevel = 0;
	public int scimitarLevel = 0;
	public int crossbowLevel = 0;

	public int bucklerXP = 0;
	public int scimitarXP = 0;
	public int crossbowXP = 0;

	private const int level1thresh = 2 - 1;
	private const int level2thresh = 4 - 1;
	private const int level3thresh = 8 - 1;
	private const int level4thresh = 16 - 1;
	private const int level5thresh = 32 - 1;

	public bool isAttacking = false;
	public bool isStunned = false;
	public string attackType = null;

	public string bucklerButton;
	public string scimitarButton;
	public string crossbowButton;
	public string xStick;

	public string facing = "left";

	public GameObject arrowPrefab;
	public GameObject crossbowBox;
	private float arrowDelay = 0.8f;
	private float timeLeft = 0;
	public float stunDelay = 1f;
	public float stunLeft = 0;

	public Animator anim;
	public GameObject cam;
	public float camShake;
	public ParticleSystem levelUpParticles;

	public GameObject xpop1;
	public GameObject xpop2;
	public GameObject xpop3;
	public GameObject xpop4;
	public GameObject xpop5;
	public GameObject ypop1;
	public GameObject ypop2;
	public GameObject ypop3;
	public GameObject ypop4;
	public GameObject ypop5;
	public GameObject bpop1;
	public GameObject bpop2;
	public GameObject bpop3;
	public GameObject bpop4;
	public GameObject bpop5;

	// Use this for initialization
	void Awake () {

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!isStunned) {
			isStunned = GetComponent<Health>().stunned;
			GetComponent<Health>().invincible = true;
			if(isStunned)
				stunLeft = stunDelay;
		}
		if (timeLeft > 0) {
			timeLeft -= Time.deltaTime;
		}

		if (stunLeft > stunDelay/2) {
			stunLeft -= Time.deltaTime;
		} else if (stunLeft <= stunDelay/2 && stunLeft > 0) {
			isStunned = false;
			stunLeft -= Time.deltaTime;
		} else if (stunLeft <= 0)
			GetComponent<Health>().invincible = false;

		if (Input.GetAxisRaw (xStick) < 0) {
			facing = "left";
		}
		if (Input.GetAxisRaw (xStick) > 0) {
			facing = "right";
		}

		// Level up checks
		switch (bucklerLevel) {
		case (5):
			break;
		case (0):
			if (bucklerXP > level1thresh){
				bucklerLevel += 1;
				cam.GetComponent<CameraShake>().shake = camShake;
				levelUpParticles.Play ();
				xpop1.SetActive(true);
			}
			break;
		case (1):
			if (bucklerXP > level2thresh){
				bucklerLevel += 1;
				cam.GetComponent<CameraShake>().shake = camShake;
				levelUpParticles.Play ();
				xpop2.SetActive(true);
			}
			break;
		case (2):
			if (bucklerXP > level3thresh){
				bucklerLevel += 1;
				cam.GetComponent<CameraShake>().shake = camShake;
				levelUpParticles.Play ();
				xpop3.SetActive(true);
			}
			break;
		case (3):
			if (bucklerXP > level4thresh){
				bucklerLevel += 1;
				cam.GetComponent<CameraShake>().shake = camShake;
				levelUpParticles.Play ();
				xpop4.SetActive(true);
			}
			break;
		case (4):
			if (bucklerXP > level5thresh){
				bucklerLevel += 1;
				cam.GetComponent<CameraShake>().shake = camShake;
				levelUpParticles.Play ();
				xpop5.SetActive(true);
			}
			break;
		default:
			break;
		}

		switch (scimitarLevel) {
		case (5):
			break;
		case (0):
			if (scimitarXP > level1thresh){
				scimitarLevel += 1;
				cam.GetComponent<CameraShake>().shake = camShake;
				levelUpParticles.Play ();
				ypop1.SetActive(true);
			}
			break;
		case (1):
			if (scimitarXP > level2thresh){
				scimitarLevel += 1;
				cam.GetComponent<CameraShake>().shake = camShake;
				levelUpParticles.Play ();
				ypop2.SetActive(true);
			}
			break;
		case (2):
			if (scimitarXP > level3thresh){
				scimitarLevel += 1;
				cam.GetComponent<CameraShake>().shake = camShake;
				levelUpParticles.Play ();
				ypop3.SetActive(true);
			}
			break;
		case (3):
			if (scimitarXP > level4thresh){
				scimitarLevel += 1;
				cam.GetComponent<CameraShake>().shake = camShake;
				levelUpParticles.Play ();
				ypop4.SetActive(true);
			}
			break;
		case (4):
			if (scimitarXP > level5thresh){
				scimitarLevel += 1;
				cam.GetComponent<CameraShake>().shake = camShake;
				levelUpParticles.Play ();
				ypop5.SetActive(true);
			}
			break;
		default:
			break;
		}

		switch (crossbowLevel) {
		case (5):
			break;
		case (0):
			if (crossbowXP > level1thresh){
				crossbowLevel += 1;
				cam.GetComponent<CameraShake>().shake = camShake;
				levelUpParticles.Play ();
				bpop1.SetActive(true);
			}
			break;
		case (1):
			if (crossbowXP > level2thresh) {
				crossbowLevel += 1;
				arrowDelay = 0.5f;
				cam.GetComponent<CameraShake>().shake = camShake;
				levelUpParticles.Play ();
				bpop2.SetActive(true);
			}
			break;
		case (2):
			if (crossbowXP > level3thresh){
				crossbowLevel += 1;
				cam.GetComponent<CameraShake>().shake = camShake;
				levelUpParticles.Play ();
				bpop3.SetActive(true);
			}
			break;
		case (3):
			if (crossbowXP > level4thresh){
				crossbowLevel += 1;
				cam.GetComponent<CameraShake>().shake = camShake;
				levelUpParticles.Play ();
				bpop4.SetActive(true);
			}
			break;
		case (4):
			if (crossbowXP > level5thresh) {
				crossbowLevel += 1;
				arrowDelay = 0.75f;
				cam.GetComponent<CameraShake>().shake = camShake;
				levelUpParticles.Play ();
				bpop5.SetActive(true);
			}
			break;
		default:
			break;
		}

		Vector3 currentVelocity = GetComponent<Rigidbody> ().velocity;
		//Input.GetButtonDown()
		if (!isAttacking && !isStunned) {
			if (Input.GetButtonDown (bucklerButton)) {
				//start attack animation
				if (facing == "left") {
					if (GetComponent<PlayerMove> ().GetGrounded ()) {
						if (bucklerLevel < 2)
							GetComponent<Rigidbody> ().velocity = currentVelocity + Vector3.left * bucklerSpeed * 0.7f;
						else
							GetComponent<Rigidbody> ().velocity = currentVelocity + Vector3.left * bucklerSpeed * 1.2f;
						attackType = "GBuckler";
						isAttacking = true;
						anim.SetBool("isCharging",true);
						anim.SetTrigger("attacking");
					} else {
						GetComponent<Rigidbody> ().velocity = currentVelocity + Vector3.left * bucklerSpeed * .2f + Vector3.down * bucklerSpeed * .4f;
						attackType = "ABuckler";
						isAttacking = true;
						anim.SetBool("isSlamming",true);
						anim.SetTrigger("attacking");
					}
				} else {
					if (GetComponent<PlayerMove> ().GetGrounded ()) {
						if (bucklerLevel < 2)
							GetComponent<Rigidbody> ().velocity = currentVelocity + Vector3.right * bucklerSpeed * 0.6f;
						else
							GetComponent<Rigidbody> ().velocity = currentVelocity + Vector3.right * bucklerSpeed * 1.4f;
						attackType = "GBuckler";
						isAttacking = true;
						
						anim.SetBool("isCharging", true);
						anim.SetTrigger("attacking");
					} else {
						GetComponent<Rigidbody> ().velocity = currentVelocity + Vector3.right * bucklerSpeed * .2f + Vector3.down * bucklerSpeed * .2f;
						attackType = "ABuckler";
						isAttacking = true;
						anim.SetBool("isSlamming", true);
						anim.SetTrigger("attacking");
					}
				}

			} else if (Input.GetButtonDown (scimitarButton)) {
				//start attack animation
				if (facing == "left") {
					if (GetComponent<PlayerMove> ().GetGrounded ()) {
						GetComponent<Rigidbody> ().velocity = currentVelocity + Vector3.left * scimitarSpeed * .2f + Vector3.up * scimitarSpeed;
						attackType = "GScimitar";
						isAttacking = true;
						anim.SetBool("isSlashing",true);
						anim.SetTrigger("attacking");
					} else {
						GetComponent<Rigidbody> ().velocity = currentVelocity + Vector3.left * scimitarSpeed * .8f + Vector3.up * scimitarSpeed * .1f;
						attackType = "AScimitar";
						isAttacking = true;
						anim.SetBool("isSwishing",true);
						anim.SetTrigger("attacking");
					}
				} else {
					if (GetComponent<PlayerMove> ().GetGrounded ()) {
						GetComponent<Rigidbody> ().velocity = currentVelocity + Vector3.right * scimitarSpeed * .2f + Vector3.up * scimitarSpeed;
						attackType = "GScimitar";
						anim.SetBool("isSlashing",true);
						anim.SetTrigger("attacking");
						isAttacking = true;
					} else {
						GetComponent<Rigidbody> ().velocity = currentVelocity + Vector3.right * bucklerSpeed * .8f + Vector3.up * scimitarSpeed * .1f;
						attackType = "AScimitar";
						isAttacking = true;
						anim.SetBool("isSwishing",true);
						anim.SetTrigger("attacking");
					}
				}
				
			} else if (Input.GetButtonDown (crossbowButton) && timeLeft <= 0 && attackType != "Crossbow") { 
				attackType = "Crossbow";
				anim.SetBool("isShooting",true);
				anim.SetTrigger("attacking");
				isAttacking = true;
				timeLeft = arrowDelay;
				GameObject arrowClone;
				GameObject arrowClone1;
				GameObject arrowClone2;

				arrowClone = (GameObject)Instantiate (arrowPrefab, new Vector3 (crossbowBox.transform.position.x, crossbowBox.transform.position.y, 0), crossbowBox.transform.rotation);
				arrowClone.GetComponent<ArrowAttack> ().player = gameObject;
				if (facing == "left") {
					arrowClone.transform.Rotate (new Vector3 (-90, 0));
				} else if (facing == "right") {
					arrowClone.transform.Rotate (new Vector3 (-90, 0));
				}
				float shotspeed = 40;
				if (crossbowLevel == 0) {
					shotspeed = 20;
				}
				if (facing == "left") {
					arrowClone.GetComponent<Rigidbody> ().velocity = new Vector3 (-1 * shotspeed, 0, 0);
					if (crossbowLevel == 5) {
						arrowClone1 = (GameObject)Instantiate (arrowPrefab, new Vector3 (crossbowBox.transform.position.x, crossbowBox.transform.position.y, 0), crossbowBox.transform.rotation);
						arrowClone1.GetComponent<ArrowAttack> ().player = gameObject;
						if (facing == "left") {
							arrowClone1.transform.Rotate (new Vector3 (-90, 0));
						} else if (facing == "right") {
							arrowClone1.transform.Rotate (new Vector3 (-90, 0));
						}
						arrowClone2 = (GameObject)Instantiate (arrowPrefab, new Vector3 (crossbowBox.transform.position.x, crossbowBox.transform.position.y, 0), crossbowBox.transform.rotation);
						arrowClone2.GetComponent<ArrowAttack> ().player = gameObject;
						if (facing == "left") {
							arrowClone2.transform.Rotate (new Vector3 (-90, 0));
						} else if (facing == "right") {
							arrowClone2.transform.Rotate (new Vector3 (-90, 0));
						}
						arrowClone1.GetComponent<Rigidbody> ().velocity = new Vector3 (-0.7f * shotspeed, 15, 0);
						arrowClone2.GetComponent<Rigidbody> ().velocity = new Vector3 (-0.7f * shotspeed, -15, 0);
					}
				} else {
					arrowClone.GetComponent<Rigidbody> ().velocity = new Vector3 (shotspeed, 0, 0);
					if (crossbowLevel == 5) {
						arrowClone1 = (GameObject)Instantiate (arrowPrefab, new Vector3 (crossbowBox.transform.position.x, crossbowBox.transform.position.y, 0), crossbowBox.transform.rotation);
						arrowClone1.GetComponent<ArrowAttack> ().player = gameObject;
						if (facing == "left") {
							arrowClone1.transform.Rotate (new Vector3 (-90, 0));
						} else if (facing == "right") {
							arrowClone1.transform.Rotate (new Vector3 (-90, 0));
						}
						arrowClone2 = (GameObject)Instantiate (arrowPrefab, new Vector3 (crossbowBox.transform.position.x, crossbowBox.transform.position.y, 0), crossbowBox.transform.rotation);
						arrowClone2.GetComponent<ArrowAttack> ().player = gameObject;
						if (facing == "left") {
							arrowClone2.transform.Rotate (new Vector3 (-90, 0));
						} else if (facing == "right") {
							arrowClone2.transform.Rotate (new Vector3 (-90, 0));
						}
						arrowClone1.GetComponent<Rigidbody> ().velocity = new Vector3 (0.7f * shotspeed, 15, 0);
						arrowClone2.GetComponent<Rigidbody> ().velocity = new Vector3 (0.7f * shotspeed, -15, 0);
					}
				}
			}
		} else if (isAttacking && attackType == "GBuckler") {//alterations here don't seem to be working...
			if (currentVelocity.x > 1 || currentVelocity.x < -1) 
			{
					currentVelocity.x *= .95f; 
				GetComponent<Rigidbody> ().velocity = currentVelocity;
			} else {
				isAttacking = false;
				anim.SetBool("isCharging",false);
				attackType = null;
			}
		} else if (isAttacking && attackType == "ABuckler") {
			if (!GetComponent<PlayerMove> ().GetGrounded ()) {
				if (currentVelocity.y < 1)
					currentVelocity.y *= 1.1f; 
				else
					currentVelocity.y = -1f;
				GetComponent<Rigidbody> ().velocity = currentVelocity;
			} else {
				isAttacking = false;
				anim.SetBool("isSlamming",false);
				attackType = null;
			}
		} else if (isAttacking && attackType == "GScimitar") {
			if (Input.GetButtonDown (scimitarButton) && scimitarLevel == 5) {
				//start attack animation
				if (facing == "left") {
					GetComponent<Rigidbody> ().velocity = currentVelocity + Vector3.left * scimitarSpeed * .5f + Vector3.up * scimitarSpeed * .1f;
					attackType = "GScimitar";
					anim.SetBool("isSlashing",true);
					anim.SetTrigger("attacking");
					isAttacking = true;
				} else {
					GetComponent<Rigidbody> ().velocity = currentVelocity + Vector3.right * bucklerSpeed * .4f;
					attackType = "GScimitar";
					anim.SetBool("isSlashing",true);
					anim.SetTrigger("attacking");
					isAttacking = true;
				} 
			} else if (currentVelocity.y > -10 && !GetComponent<PlayerMove> ().GetGrounded () ||currentVelocity.y > 0) {
				if (currentVelocity.y > 0)
				{
				if (scimitarLevel < 2)
					currentVelocity.y *= .9f;
				else
					currentVelocity.y *= .94f; 
				}
				GetComponent<Rigidbody> ().velocity = currentVelocity;
			} else {
				isAttacking = false;
				anim.SetBool("isSlashing",false);
				attackType = null;
			}
		} else if (isAttacking && attackType == "AScimitar") {
			if (!GetComponent<PlayerMove> ().GetGrounded ()) 
			{
				if (scimitarLevel < 3)
					currentVelocity.x *= .92f;
				else
					currentVelocity.x *= .99f;
				GetComponent<Rigidbody> ().velocity = currentVelocity;
			}
			else{
				isAttacking = false;
				anim.SetBool("isSwishing",false);
				attackType = null;
			}
		} else if (isAttacking && attackType == "Crossbow") {
			isAttacking = false;
			anim.SetBool("isShooting",false);
			attackType = null;
		}
	}

	public void reset()
	{
		bucklerLevel = 0;
		scimitarLevel = 0;
		crossbowLevel = 0;
		bucklerXP = 0;
		scimitarXP = 0;
		crossbowXP = 0;
		arrowDelay = 0.8f;
	}
}
