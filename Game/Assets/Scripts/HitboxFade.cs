﻿using UnityEngine;
using System.Collections;

public class HitboxFade : MonoBehaviour {
	private Renderer render;
	private Color colour;
	private bool active;
	public GameObject player;
	// Use this for initialization
	void Awake () {
		render = GetComponent<Renderer>();
		colour = render.material.color;
		colour.a = 0f;
		render.material.color = colour;
		active = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (player.GetComponent<AttackBehaviours> ().attackType == GetComponent<HitBoxTrigger> ().attackMode) {
			if (!active) {
				active = true;
				colour.a = 1f;
				render.material.color = colour;
			}
		} 
		else 
		{
			if (active)
			{
				active = false;
				colour.a = 0f;
				render.material.color = colour;
			}
		}
	}
}
