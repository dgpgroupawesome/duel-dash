﻿using UnityEngine;
using System.Collections;

public class ParallaxScroll : MonoBehaviour {

	public float speed = 0; // scroll speed of parallax scroll
	
	// Update is called once per frame
	void Update ()
	{
		GetComponent<Renderer>().material.mainTextureOffset = new Vector2 ((Time.time * speed) % 1, 0f);
	}
}
