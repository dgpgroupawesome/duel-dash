﻿using UnityEngine;
using System.Collections;

public class Arenascript : MonoBehaviour {

	public GameObject player1;
	public GameObject player2;
	public GameObject camera1;
	public GameObject camera2;
	public GameObject camera3;

	//UI components
	public GameObject progress;
	public GameObject counter1;
	public GameObject counter2;
	public GameObject counter3;
	public GameObject countergo;
	public GameObject winner1;
	public GameObject winner2;
	public GameObject point1;
	public GameObject point2;
	public GameObject tie;

	public GameObject tutorialmessage1;
	public GameObject tutorialmessage2;
	public GameObject tutorialmessage3;
	public GameObject tutorialmessage4;
	public GameObject tutorialmessage5;
	public GameObject tutorialmessagebackground;

	public GameObject tutorialleveltheme;
	public GameObject leadleveltheme;
	public GameObject leveltheme1;
	public GameObject leveltheme2;
	public GameObject leveltheme3;
	public GameObject victorytheme;
	public GameObject menutheme;

	public GameObject prompt1;
	public GameObject prompt2;

	private bool p1ready = false;
	private bool p2ready = false;

	private int p1score = 0;
	private int p2score = 0;

	private Vector3 numberstart;
	private Vector3 redstart;
	private Vector3 bluestart;

	public float timer = 0;
	private int stattimer = 0;
	public float musictimer = 0;

	public string arenaState = "menu";

	private CharacterFollower redCF;
	private CharacterFollower blueCF;

	public MenuController menu;

	private int tutorialmessage = 1;

	void Start ()
	{
		player1.GetComponent<AttackBehaviours> ().stunLeft = 4f;
		player1.GetComponent<AttackBehaviours> ().isStunned = true;
		player2.GetComponent<AttackBehaviours> ().stunLeft = 4f;
		player2.GetComponent<AttackBehaviours> ().isStunned = true;
		timer = 4f;
		stattimer = 4;
		numberstart = counter1.GetComponent<RectTransform> ().position;
		redstart = player1.GetComponent<Transform> ().position;
		bluestart = player2.GetComponent<Transform> ().position;

		blueCF = progress.transform.Find("Blue").GetComponent<CharacterFollower>();
		redCF = progress.transform.Find("Red").GetComponent<CharacterFollower>();
	}
	// Update is called once per frame
	void Update () 
	{
		if (timer > 0)
			timer -= Time.deltaTime;
		if (musictimer > 0)
			musictimer -= Time.deltaTime;
		if (arenaState == "menu") {
			player1.GetComponent<AttackBehaviours> ().stunLeft = 30f;
			player1.GetComponent<AttackBehaviours> ().isStunned = true;
			player2.GetComponent<AttackBehaviours> ().stunLeft = 30f;
			player2.GetComponent<AttackBehaviours> ().isStunned = true;
			timer = 5f;
			stattimer = 4;
		}
		if (arenaState == "tutorial") {
			if (tutorialmessage == 1)
			{
				if (timer <= 19.1)
				{
					tutorialmessage = 2;
					tutorialmessage2.SetActive(true);
					tutorialmessage1.SetActive(false);
				}
				else
				{
					tutorialmessage1.SetActive(true);
					tutorialmessagebackground.SetActive(true);
					winner1.SetActive(false);
					winner2.SetActive(false);
				}
			}
			else if (tutorialmessage == 2)
			{
				if (timer <= 15.45)
				{
					tutorialmessage = 3;
					tutorialmessage3.SetActive(true);
					tutorialmessage2.SetActive(false);

				}
			}
			else if (tutorialmessage == 3)
			{
				if (timer <= 11.8)
				{
					tutorialmessage = 4;
					tutorialmessage4.SetActive(true);
					tutorialmessage3.SetActive(false);
				}
			}
			else if (tutorialmessage == 4)
			{
				if (timer <= 8.15)
				{
					tutorialmessage = 5;
					tutorialmessage5.SetActive(true);
					tutorialmessage4.SetActive(false);
				}
			}
			else if (tutorialmessage == 5)
			{
				if (timer <= 4.0)
				{
					tutorialmessage = 1;
					tutorialmessage5.SetActive(false);
					tutorialmessagebackground.SetActive(false);
					arenaState = "start";
				}
			}
			if(menutheme.GetComponent<AudioSource>().isPlaying){
				menutheme.GetComponent<AudioSource>().Stop();
				tutorialleveltheme.GetComponent<AudioSource>().Play();
				musictimer = tutorialleveltheme.GetComponent<AudioSource>().clip.length;
			}
		}
		if (arenaState == "start") {
			if(menutheme.GetComponent<AudioSource>().isPlaying){
				menutheme.GetComponent<AudioSource>().Stop();
				leadleveltheme.GetComponent<AudioSource>().Play();
				musictimer = leadleveltheme.GetComponent<AudioSource>().clip.length;
			}
			   
			Vector3 tempy = numberstart;
			if (timer <= 0)
			{
				timer = 2;
				stattimer = 0;
				counter1.SetActive(false);
				countergo.SetActive(true);
				arenaState = "levelling";
				player1.GetComponent<AttackBehaviours> ().stunLeft = 0f;
				player1.GetComponent<AttackBehaviours> ().isStunned = false;
				player2.GetComponent<AttackBehaviours> ().stunLeft = 0f;
				player2.GetComponent<AttackBehaviours> ().isStunned = false;
			}
			else
			{
				if (stattimer > 3)
				{
					if (timer < 3)
					{
						stattimer = 3;
						counter3.SetActive(true);
						counter3.GetComponent<RectTransform>().position = numberstart;
						point1.SetActive(false);
						point2.SetActive(false);
						tie.SetActive(false);
						winner1.SetActive(false);
						winner2.SetActive(false);
						progress.SetActive (true);
					}
				}
				else if (stattimer > 2)
				{
					tempy = counter3.GetComponent<RectTransform>().position;
					tempy.y += 54 * Time.deltaTime;
					counter3.GetComponent<RectTransform>().position = tempy;
					if (timer < 2)
					{
						stattimer = 2;
						counter3.SetActive(false);
						counter2.SetActive(true);
						counter2.GetComponent<RectTransform>().position = numberstart;
					}
				}
				else if (stattimer > 1)
				{
					tempy = counter2.GetComponent<RectTransform>().position;
					tempy.y += 54 * Time.deltaTime;
					counter2.GetComponent<RectTransform>().position = tempy;
					if (timer < 1)
					{
						stattimer = 1;
						counter2.SetActive(false);
						counter1.SetActive(true);
						counter1.GetComponent<RectTransform>().position = numberstart;
					}
				}
				else if (stattimer > 0)
				{
					tempy = counter1.GetComponent<RectTransform>().position;
					tempy.y += 54 * Time.deltaTime;
					counter1.GetComponent<RectTransform>().position = tempy;
				}
			}
		}
		else if (arenaState == "levelling") 
		{
			if (timer <= 0)
			{
				countergo.SetActive(false);
			}
			if (musictimer <= 0 && !leveltheme1.GetComponent<AudioSource>().isPlaying && !leveltheme2.GetComponent<AudioSource>().isPlaying)
			{
				leveltheme1.GetComponent<AudioSource>().Play();
			}
			if (!p1ready)
			{
				if (player1.GetComponent<Transform>().position.x > 0)
				{
					player1.GetComponent<Transform>().position = new Vector3(0, 60, 0);
					p1ready = true;
					redCF.frozen = true;
					if (!leveltheme2.GetComponent<AudioSource>().isPlaying)
					{
						leveltheme1.GetComponent<AudioSource>().Stop();
						tutorialleveltheme.GetComponent<AudioSource>().Stop();
						leadleveltheme.GetComponent<AudioSource>().Stop();
						leveltheme2.GetComponent<AudioSource>().Play();
					}
				}
			}
			if (!p2ready)
			{
				if (player2.GetComponent<Transform>().position.x < 0)
				{
					player2.GetComponent<Transform>().position = new Vector3(0, 60, 0);
					p2ready = true;
					blueCF.frozen = true;
					if (!leveltheme2.GetComponent<AudioSource>().isPlaying)
					{
						leveltheme1.GetComponent<AudioSource>().Stop();
						tutorialleveltheme.GetComponent<AudioSource>().Stop();
						leadleveltheme.GetComponent<AudioSource>().Stop();
						leveltheme2.GetComponent<AudioSource>().Play();
					}
				}
			}
			if (p1ready && p2ready)
			{
				camera1.SetActive(false);
				camera2.SetActive(false);
				camera3.SetActive(true);
				progress.SetActive(false);
				arenaState = "battling";
				leveltheme2.GetComponent<AudioSource>().Stop();
				leveltheme3.GetComponent<AudioSource>().Play();
			}
		}
		if (arenaState == "battling")
			if (player1.GetComponent<Health> ().currentHealth < 1 || player2.GetComponent<Health> ().currentHealth < 1) 
			{
				arenaState = "round over";
				if (player1.GetComponent<Health> ().currentHealth < 1)
				{
					p2score += 1;
				}
				else
				{
					p1score += 1;
				}
			}
		if (arenaState == "round over") {
			if (p1score == 2 || p2score == 2) {
				arenaState = "game over";
				leveltheme3.GetComponent<AudioSource> ().Stop ();
				victorytheme.GetComponent<AudioSource> ().Play ();
				timer = 12f;
				if (p1score == 2)
					winner1.SetActive (true);
				else
					winner2.SetActive (true);
			} else {
				if (p1score == 1) {
					if (p2score == 1) {
						tie.SetActive (true);
					} else {
						point1.SetActive (true);
					}
				} else {
					point2.SetActive (true);
				}
				player1.GetComponent<AttackBehaviours> ().stunLeft = 5f;
				player1.GetComponent<AttackBehaviours> ().isStunned = true;
				player2.GetComponent<AttackBehaviours> ().stunLeft = 5f;
				player2.GetComponent<AttackBehaviours> ().isStunned = true;
				player1.GetComponent<Health> ().currentHealth = 10;
				player2.GetComponent<Health> ().currentHealth = 10;
				player1.GetComponent<Transform> ().position = redstart;
				player2.GetComponent<Transform> ().position = bluestart;
				timer = 5f;
				stattimer = 5;
				p1ready = false;
				p2ready = false;
				camera1.SetActive (true);
				camera2.SetActive (true);
				camera3.SetActive (false);
				progress.SetActive (true);
				leveltheme3.GetComponent<AudioSource>().Stop();
				leadleveltheme.GetComponent<AudioSource>().Play();
				musictimer = leadleveltheme.GetComponent<AudioSource>().clip.length;
				redCF.frozen = false;
				blueCF.frozen = false;
				arenaState = "start";
			}
		} 
		if (arenaState == "game over") {
			if (timer <= 0)
			{
				stattimer = 5;
				player1.GetComponent<AttackBehaviours> ().stunLeft = 5f;
				player1.GetComponent<AttackBehaviours> ().isStunned = true;
				player2.GetComponent<AttackBehaviours> ().stunLeft = 5f;
				player2.GetComponent<AttackBehaviours> ().isStunned = true;
				player1.GetComponent<Health> ().currentHealth = 10;
				player2.GetComponent<Health> ().currentHealth = 10;
				player1.GetComponent<Transform> ().position = redstart;
				player2.GetComponent<Transform> ().position = bluestart;
				p1ready = false;
				p2ready = false;
				camera1.SetActive (true);
				camera2.SetActive (true);
				camera3.SetActive (false);
				redCF.frozen = false;
				blueCF.frozen = false;
				p1score = 0;
				p2score = 0;
				player1.GetComponent<AttackBehaviours>().reset();
				player2.GetComponent<AttackBehaviours>().reset();
				prompt1.GetComponent<CharacterFollower>().resetAll();
				prompt2.GetComponent<CharacterFollower>().resetAll();
				menu.showMenu();
				victorytheme.GetComponent<AudioSource>().Stop();
				menutheme.GetComponent<AudioSource>().Play();
				arenaState = "menu";
				winner1.SetActive(false);
				winner2.SetActive(false);
			}
		}
	}
}
