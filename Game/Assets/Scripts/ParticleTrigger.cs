﻿using UnityEngine;
using System.Collections;

public class ParticleTrigger : MonoBehaviour {

	public GameObject Dust;

	void Start ()
	{
	}

	void OnTriggerEnter()
	{
		Dust.GetComponent<ParticleSystem> ().Play ();
	}

	void OnTriggerExit()
	{
	}
}