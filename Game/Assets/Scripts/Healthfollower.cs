﻿using UnityEngine;
using System.Collections;

public class Healthfollower : MonoBehaviour {

	public GameObject Player;
	public string Color;
	public float newx;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Color == "blue") {
			newx = Player.GetComponent<Health>().currentHealth * 5 + 10;
		}
		else
		{
			newx = -Player.GetComponent<Health>().currentHealth * 5;
		}
		GetComponent<RectTransform>().sizeDelta = new Vector2 ((Player.GetComponent<Health>().currentHealth)*11, GetComponent<RectTransform>().rect.height);
		GetComponent<RectTransform>().position = new Vector3 (newx/10, GetComponent<RectTransform>().position.y, GetComponent<RectTransform>().position.z);
	}
}
