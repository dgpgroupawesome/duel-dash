﻿using UnityEngine;
using System.Collections;

public class SpriteRotate : MonoBehaviour {
	public GameObject player;
	public int pNumber;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if ((player.transform.localRotation.eulerAngles.y < 180 && pNumber == 2) || (player.transform.localRotation.eulerAngles.y > 260 && pNumber == 1)){ //&& !(transform.localRotation.eulerAngles.y > 50 && transform.localRotation.eulerAngles.y < 100))){
			transform.localScale = new Vector3(1,1,1);
			transform.localEulerAngles = new Vector3(0,-90,0);
		}
		else if ((player.transform.localRotation.eulerAngles.y > 180 && pNumber == 2) || (player.transform.localRotation.eulerAngles.y < 100 && pNumber == 1)){ //&& !(transform.localRotation.eulerAngles.y > 50 && transform.localRotation.eulerAngles.y < 100))){
			transform.localScale = new Vector3(-1,1,1);
			transform.localEulerAngles = new Vector3(0,90,0);
		}
		if ((player.transform.localRotation.eulerAngles.y < 180 && pNumber == 1)){ //&& !(transform.localRotation.eulerAngles.y > 50 && transform.localRotation.eulerAngles.y < 100))){
			transform.localScale = new Vector3(1,1,1);
			transform.localEulerAngles = new Vector3(0,-90,0);
		}
		else if ((player.transform.localRotation.eulerAngles.y > 180 && pNumber == 1)){ //&& !(transform.localRotation.eulerAngles.y > 50 && transform.localRotation.eulerAngles.y < 100))){
			transform.localScale = new Vector3(-1,1,1);
			transform.localEulerAngles = new Vector3(0,90,0);
		}
	}
}
