﻿using UnityEngine;
using System.Collections;

public class CharacterFollower : MonoBehaviour {

	public GameObject Player;
	public GameObject X0;
	public GameObject X1;
	public GameObject X2;
	public GameObject X3;
	public GameObject X4;
	public GameObject X5;
	public GameObject Y0;
	public GameObject Y1;
	public GameObject Y2;
	public GameObject Y3;
	public GameObject Y4;
	public GameObject Y5;
	public GameObject B0;
	public GameObject B1;
	public GameObject B2;
	public GameObject B3;
	public GameObject B4;
	public GameObject B5;
	public bool frozen;
	private int xlevel = 0;
	private int ylevel = 0;
	private int blevel = 0;

	// Use this for initialization
	void Start () {
		frozen = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (tag == "Prompt") {
			switch(xlevel)
			{
			case 0:
				if (Player.GetComponent<AttackBehaviours>().bucklerLevel > 0)
				{
					xlevel = 1;
					X1.SetActive(true);
					X0.SetActive(false);
				}
				break;
			case 1:
				if (Player.GetComponent<AttackBehaviours>().bucklerLevel > 1)
				{
					xlevel = 2;
					X2.SetActive(true);
					X1.SetActive(false);
				}
				break;
			case 2:
				if (Player.GetComponent<AttackBehaviours>().bucklerLevel > 2)
				{
					xlevel = 3;
					X3.SetActive(true);
					X2.SetActive(false);
				}
				break;
			case 3:
				if (Player.GetComponent<AttackBehaviours>().bucklerLevel > 3)
				{
					xlevel = 4;
					X4.SetActive(true);
					X3.SetActive(false);
				}
				break;
			case 4:
				if (Player.GetComponent<AttackBehaviours>().bucklerLevel > 4)
				{
					xlevel = 5;
					X5.SetActive(true);
					X4.SetActive(false);
				}
				break;
			}
			switch(ylevel)
			{
			case 0:
				if (Player.GetComponent<AttackBehaviours>().scimitarLevel > 0)
				{
					ylevel = 1;
					Y1.SetActive(true);
					Y0.SetActive(false);
				}
				break;
			case 1:
				if (Player.GetComponent<AttackBehaviours>().scimitarLevel > 1)
				{
					ylevel = 2;
					Y2.SetActive(true);
					Y1.SetActive(false);
				}
				break;
			case 2:
				if (Player.GetComponent<AttackBehaviours>().scimitarLevel > 2)
				{
					ylevel = 3;
					Y3.SetActive(true);
					Y2.SetActive(false);
				}
				break;
			case 3:
				if (Player.GetComponent<AttackBehaviours>().scimitarLevel > 3)
				{
					ylevel = 4;
					Y4.SetActive(true);
					Y3.SetActive(false);
				}
				break;
			case 4:
				if (Player.GetComponent<AttackBehaviours>().scimitarLevel > 4)
				{
					ylevel = 5;
					Y5.SetActive(true);
					Y4.SetActive(false);
				}
				break;
			}
			switch(blevel)
			{
			case 0:
				if (Player.GetComponent<AttackBehaviours>().crossbowLevel > 0)
				{
					blevel = 1;
					B1.SetActive(true);
					B0.SetActive(false);
				}
				break;
			case 1:
				if (Player.GetComponent<AttackBehaviours>().crossbowLevel > 1)
				{
					blevel = 2;
					B2.SetActive(true);
					B1.SetActive(false);
				}
				break;
			case 2:
				if (Player.GetComponent<AttackBehaviours>().crossbowLevel > 2)
				{
					blevel = 3;
					B3.SetActive(true);
					B2.SetActive(false);
				}
				break;
			case 3:
				if (Player.GetComponent<AttackBehaviours>().crossbowLevel > 3)
				{
					blevel = 4;
					B4.SetActive(true);
					B3.SetActive(false);
				}
				break;
			case 4:
				if (Player.GetComponent<AttackBehaviours>().crossbowLevel > 4)
				{
					blevel = 5;
					B5.SetActive(true);
					B4.SetActive(false);
				}
				break;
			}
		} else {
			if (!frozen) {
				Vector3 tempx = GetComponent<RectTransform> ().position;
				tempx.x = (Player.GetComponent<Transform> ().position.x) * 2.5f + Screen.width / 2;
				GetComponent<RectTransform> ().position = tempx;
			} else {
				Vector3 tempx = GetComponent<RectTransform> ().position;
				tempx.x = Screen.width / 2;
				GetComponent<RectTransform> ().position = tempx;
			}
		}
	}

	public void resetAll() {
		X0.SetActive (true);
		Y0.SetActive (true);
		B0.SetActive (true);
		X1.SetActive (false);
		X2.SetActive (false);
		X3.SetActive (false);
		X4.SetActive (false);
		X5.SetActive (false);
		Y1.SetActive (false);
		Y2.SetActive (false);
		Y3.SetActive (false);
		Y4.SetActive (false);
		Y5.SetActive (false);
		B1.SetActive (false);
		B2.SetActive (false);
		B3.SetActive (false);
		B4.SetActive (false);
		B5.SetActive (false);
		xlevel = 0;
		ylevel = 0;
		blevel = 0;
	}
}
