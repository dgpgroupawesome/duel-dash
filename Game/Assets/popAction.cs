﻿using UnityEngine;
using System.Collections;

public class popAction : MonoBehaviour {

	private float timer;
	private Vector3 temppos;
	private float extraheight;

	public GameObject player;

	// Use this for initialization
	void Start () {
	}

	void OnEnable() {
		timer = 2f;
		temppos = player.GetComponent<Transform> ().position;
		temppos.y += 1;
		gameObject.GetComponent<Transform> ().position = temppos;
		extraheight = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.deltaTime < 1) {
			extraheight += Time.deltaTime * 3;
		} else {
			extraheight = 0;
		}
		temppos = player.GetComponent<Transform> ().position;
		temppos.y += 1 + extraheight;
		gameObject.GetComponent<Transform> ().position = temppos;
		if (timer > 0) {
			timer -= Time.deltaTime;
		} else {
			gameObject.SetActive(false);
		}
	}
}
